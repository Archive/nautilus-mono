/*
 *  nautilus-mono.c - Nautilus Mono extension
 * 
 *  Copyright (C) 2003, 2004 Novell, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Calvin Gaisford <cgaisford@novell.com>
 *          Dave Camp <dave@ximian.com>
 * 
 */

#ifndef NAUTILUS_MONO_H
#define NAUTILUS_MONO_H

#include <mono/metadata/debug-helpers.h>
#include <glib-object.h>

#define NAUTILUS_TYPE_OBJECT_LIST (nautilus_object_list_get_type ())
GType nautilus_object_list_get_type (void);


MonoDomain *nautilus_mono_get_domain (void);

MonoAssembly *nautilus_mono_get_nautilus_assembly (void);
MonoAssembly *nautilus_mono_get_glib_assembly (void);
MonoAssembly *nautilus_mono_get_gtk_assembly (void);

#endif
