
namespace Nautilus {
	using System;
	using System.Collections;
	using System.Runtime.InteropServices;

	public class File : GLib.Object, FileInfo {
		~File () {
			Dispose ();
		}

		public File (IntPtr raw) : base (raw) {}

		//protected File (Glib.GType gtype) : base (gtype) {}

		[DllImport ("libnautilus-extension.so")]
		static extern bool nautilus_file_info_is_gone (IntPtr raw);
		public bool IsGone {
			get {
				return nautilus_file_info_is_gone (Handle);
			}
		}

		[DllImport ("libnautilus-extension.so")]
		static extern bool nautilus_file_info_is_directory (IntPtr raw);
		public bool IsDirectory {
			get {
				return nautilus_file_info_is_directory (Handle);
			}
		}
		
		[DllImport ("libnautilus-extension.so")]
		static extern IntPtr nautilus_file_info_get_mime_type (IntPtr raw);
		public string MimeType {
			get {
				IntPtr raw_ret = nautilus_file_info_get_mime_type (Handle);
				string ret = GLib.Marshaller.PtrToStringGFree (raw_ret);
				return ret;
			}
		}
		
		[DllImport ("libnautilus-extension.so")]
		static extern IntPtr nautilus_file_info_get_uri_scheme (IntPtr raw);
		public string UriScheme {
			get {
				IntPtr raw_ret = nautilus_file_info_get_uri_scheme (Handle);
				string ret = GLib.Marshaller.PtrToStringGFree (raw_ret);
				return ret;
			}
		}
		
		[DllImport ("libnautilus-extension.so")]
		static extern IntPtr nautilus_file_info_get_uri (IntPtr raw);
		public string Uri {
			get {
				IntPtr raw_ret = nautilus_file_info_get_uri (Handle);
				string ret = GLib.Marshaller.PtrToStringGFree (raw_ret);
				return ret;
			}
		}

		[DllImport ("libnautilus-extension.so")]
		static extern IntPtr nautilus_file_info_get_name (IntPtr raw);
		public string Name {
			get {
				IntPtr raw_ret = nautilus_file_info_get_name (Handle);
				string ret = GLib.Marshaller.PtrToStringGFree (raw_ret);
				return ret;
			}
		}

		[DllImport ("libnautilus-extension.so")]
		static extern IntPtr nautilus_file_info_get_parent_uri (IntPtr raw);
		public string ParentUri {
			get {
				IntPtr raw_ret = nautilus_file_info_get_parent_uri (Handle);
				string ret = GLib.Marshaller.PtrToStringGFree (raw_ret);
				return ret;
			}
		}

		[DllImport ("libnautilus-extension.so")]
		static extern IntPtr nautilus_file_info_get_string_attribute (IntPtr raw, string attribute_name);
		public string GetStringAttribute (string attribute_name) {
			IntPtr raw_ret = nautilus_file_info_get_string_attribute (Handle, attribute_name);
			string ret = GLib.Marshaller.PtrToStringGFree (raw_ret);
			return ret;
		}

		[DllImport ("libnautilus-extension.so")]
		static extern void nautilus_file_info_add_string_attribute (IntPtr raw, string attribute_name, string value);
		public void AddStringAttribute (string attribute_name, 
						string value) {
			nautilus_file_info_add_string_attribute (Handle, attribute_name, value);
		}

		[DllImport ("libnautilus-extension.so")]
		static extern void nautilus_file_info_invalidate_extension_info (IntPtr raw);
		public void InvalidateExtensionInfo () {
			nautilus_file_info_invalidate_extension_info (Handle);
		}

		[DllImport ("libnautilus-extension.so")]
		static extern void nautilus_file_info_add_emblem (IntPtr raw, string emblem_name);
		public void AddEmblem (string emblem_name) {
			nautilus_file_info_add_emblem (Handle, emblem_name);
		}

		[DllImport ("libnautilus-extension.so")]
		static extern bool nautilus_file_info_is_mime_type (IntPtr raw, string mime_type);
		public bool IsMimeType (string mime_type) {
			return nautilus_file_info_is_mime_type (Handle, mime_type);
		}
	}
}
