#ifndef MONO_HELPERS_H
#define MONO_HELPERS_H

#include <glib-object.h>
#include <mono/jit/jit.h>

/* Some helpers for dealing with the fact that all mono objects
 * must be on the heap */
typedef struct {
	MonoObject *object;
} NautilusMonoHandle;

NautilusMonoHandle* nautilus_mono_handle_new (void);
void                nautilus_mono_handle_free (NautilusMonoHandle *handle);


gboolean nautilus_mono_handle_get_value (NautilusMonoHandle *handle,
					 GValue *value);


/* marshalling helpers */
void nautilus_mono_marshal_register_boxed_type (GType type,
						MonoAssembly *assembly,
						const char *namespace,
						const char *class_name);

void nautilus_mono_marshal_register_enum_type (GType type,
					       MonoAssembly *assembly,
					       const char *namespace,
					       const char *class_name);

NautilusMonoHandle *
nautilus_mono_invoke_with_values (NautilusMonoHandle *handle,
				  const char *method_desc,
				  gboolean use_namespace,
				  GList *values);

NautilusMonoHandle *
nautilus_mono_invoke (NautilusMonoHandle *handle,
		      const char *method_desc,
		      gboolean use_namespace,
		      GType first_type,
		      ...);

#endif
