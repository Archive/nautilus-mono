#include <config.h>
#include "nautilus-mono-helpers.h"

#include <glib.h>
#include <glib-object.h>
#include <gobject/gvaluecollector.h>
#include <string.h>

#include <mono/jit/jit.h>
#include <mono/metadata/debug-helpers.h>
#include <mono/metadata/threads.h>
#include <mono/metadata/tabledefs.h>

#include "nautilus-mono.h"

NautilusMonoHandle *
nautilus_mono_handle_new (void)
{
	return g_new0 (NautilusMonoHandle, 1);
}

void
nautilus_mono_handle_free (NautilusMonoHandle *handle)
{
	if (handle) {
		handle->object = NULL;
		g_free (handle);
	}
}


static GHashTable *
get_boxed_type_hash (void)
{
	static GHashTable *types = NULL;
	if (!types) {
		types = g_hash_table_new (g_direct_hash,
					  g_direct_equal);
	}

	return types;
}

static GHashTable *
get_boxed_class_hash (void)
{
	static GHashTable *types = NULL;
	if (!types) {
		types = g_hash_table_new (g_direct_hash,
					  g_direct_equal);
	}

	return types;
}

static GHashTable *
get_enum_type_hash (void)
{
	static GHashTable *types = NULL;
	if (!types) {
		types = g_hash_table_new (g_direct_hash,
					  g_direct_equal);
	}

	return types;
}

static GHashTable *
get_enum_class_hash (void)
{
	static GHashTable *types = NULL;
	if (!types) {
		types = g_hash_table_new (g_direct_hash,
					  g_direct_equal);
	}

	return types;
}

static NautilusMonoHandle *
get_property (NautilusMonoHandle *handle,
	      const char *property_name)
{
	MonoProperty *property;
	NautilusMonoHandle *boxed;
	NautilusMonoHandle *exception;
	
	property = mono_class_get_property_from_name (handle->object->vtable->klass,
						      property_name);
	if (!property) {
		return NULL;
	}	

	boxed = nautilus_mono_handle_new ();
	exception = nautilus_mono_handle_new ();
	
	boxed->object = mono_property_get_value (property, handle->object, 
						 NULL, &exception->object);

	if (exception->object) {
		mono_print_unhandled_exception (exception->object);
		nautilus_mono_handle_free (boxed);
		nautilus_mono_handle_free (exception);		
		return NULL;
	} else {
		nautilus_mono_handle_free (exception);
		return boxed;
	}
}


static GType 
get_object_type (NautilusMonoHandle *handle)
{
	NautilusMonoHandle *type_object;
	NautilusMonoHandle *boxed;
	GType ret;
	
	type_object = get_property (handle, "GType");

	if (!type_object) {
		return 0;
	}
	
	boxed = get_property (handle, "Val");
	if (!boxed) {
		g_warning ("GType class did not have a Val property\n");
		return 0;
	}
	
	ret = *(GType*)mono_object_unbox (boxed->object);
	nautilus_mono_handle_free (boxed);	

	return ret;
}

/* Get the Handle from a GLib.IWrapper object */
static gpointer
get_wrapped_object (NautilusMonoHandle *handle)
{
	NautilusMonoHandle *boxed;
	gpointer ret;	

	boxed = get_property (handle, "Handle");

	if (!boxed) {
		g_warning ("tried to get_wrapped_object on an object without a Handle property\n");
		return NULL;
	} 
	
	ret = *(gpointer*)mono_object_unbox (boxed->object);
	nautilus_mono_handle_free (boxed);
	
	return ret;
	
}

gboolean
nautilus_mono_handle_get_value (NautilusMonoHandle *handle,
				GValue *value)
{
	GType type;
	
	g_return_val_if_fail (handle->object != NULL, FALSE);

	type = get_object_type (handle);
	
	if (type != 0) {
		g_value_init (value, type);
		g_value_set_object (value, get_wrapped_object (handle));
		return TRUE;
	}

	type = (GType)g_hash_table_lookup (get_boxed_class_hash(), 
					   handle->object->vtable->klass);

	if (type) {
		g_return_val_if_fail (G_TYPE_IS_BOXED (type), FALSE);
		g_value_init (value, type);
		g_value_set_boxed (value, get_wrapped_object (handle));
		return TRUE;
	}
	
	if (handle->object->vtable->klass->enumtype) {
		type = (GType)g_hash_table_lookup (get_enum_class_hash(), 
						   handle->object->vtable->klass);
		g_return_val_if_fail (type != 0, FALSE);
			
		g_value_init (value, type);
		g_value_set_enum (value, *(int*)mono_object_unbox (handle->object));
		return TRUE;
	} 

	g_warning ("unimplemented value type!");
	return FALSE;
}

static MonoMethod *
lookup_method_desc (MonoClass *class, 
		    const char *name,
		    gboolean use_namespace)
{
	MonoMethodDesc *desc;
	MonoMethod *ret;

	desc = mono_method_desc_new (name, use_namespace);
	
	ret = NULL;
	if (desc) {
		ret = mono_method_desc_search_in_class (desc, class);
		mono_method_desc_free (desc);
	} else {
		g_warning ("couldn't parse method desc %s", name);
	}

	return ret;
}

static MonoMethod *
lookup_static_desc (MonoAssembly *assembly, 
		    const char *name,
		    gboolean use_namespace)
{
	MonoMethodDesc *desc;
	MonoMethod *ret;

	desc = mono_method_desc_new (name, use_namespace);
	
	ret = NULL;
	if (desc) {
		ret = mono_method_desc_search_in_image (desc, 
							assembly->image);
		mono_method_desc_free (desc);
	} else {
		g_warning ("couldn't parse method desc %s", name);
	}

	return ret;
}

static NautilusMonoHandle *
invoke_print_exceptions (MonoObject *object,
			 MonoMethod *method, 
			 void **params) 
{
	NautilusMonoHandle *ret;
	NautilusMonoHandle *exception;

	ret = nautilus_mono_handle_new ();
	exception = nautilus_mono_handle_new ();

	exception->object = NULL;
	
	ret->object = mono_runtime_invoke (method, 
					   object, 
					   params,
					   &exception->object);

	if (exception->object) {
		mono_print_unhandled_exception (exception->object);

		nautilus_mono_handle_free (exception);
		nautilus_mono_handle_free (ret);
		return NULL;
	} else {
		nautilus_mono_handle_free (exception);
		return ret;
	}
}

static NautilusMonoHandle *
wrap_object (gpointer handle)
{
	MonoMethod *method;
	NautilusMonoHandle *ret;
	void *args[1];

	method = lookup_static_desc (nautilus_mono_get_glib_assembly (), 
				     "GLib.Object:GetObject(intptr)", 
				     TRUE);
	if (!method) {
		g_warning ("Couldn't look up GLib.Object:GetObject method");
		return NULL;
	}
	
	args[0] = &handle;

	ret = invoke_print_exceptions (NULL, method, args);

	return ret;
}

static NautilusMonoHandle *
wrap_boxed (gpointer handle,
	    GType type)
{
	MonoMethod *method;
	MonoClass *class;
	NautilusMonoHandle *obj;
	NautilusMonoHandle *ret;
	void *args[1];
	char *constructor;
	
	g_return_val_if_fail (type != 0, NULL);

	class = g_hash_table_lookup (get_boxed_type_hash (),
				     (gpointer)type);
	if (!class) {
		g_warning ("trying to wrap unregistered boxed type");
		return NULL;
	}
		
	constructor = g_strdup_printf ("%s.%s::.ctor(intptr)", 
				       class->name_space, 
				       class->name);
	method = lookup_method_desc (class, constructor, TRUE);
	g_free (constructor);
	
	if (!method) {
		g_warning ("couldn't find constructor for %s.%s(intptr)",
			   class->name_space, class->name);
		return NULL;
	}

	obj = nautilus_mono_handle_new ();
	obj->object = mono_object_new (nautilus_mono_get_domain (), class);

	if (!obj->object) {
		g_warning ("couldn't create mono object\n");
		return NULL;
	}

	args[0] = &handle;
	
	ret = invoke_print_exceptions (obj->object, method, args);

	if (ret) {
		/* We don't care about the return value */
		nautilus_mono_handle_free (ret);

		return obj;
	} else {
		g_warning ("couldn't invoke constructor");
		nautilus_mono_handle_free (obj);
		return NULL;
	}
}

void
nautilus_mono_marshal_register_boxed_type (GType type,
					   MonoAssembly *assembly,
					   const char *namespace,
					   const char *class_name)
{
	MonoClass *class;

	g_return_if_fail (G_TYPE_IS_BOXED (type));
	
	class = mono_class_from_name (assembly->image,
				      namespace,
				      class_name);
	if (!class) {
		g_warning ("couldn't find class for %s.%s", 
			   namespace, class_name);
		return;
	}

	mono_class_init (class);

	g_hash_table_insert (get_boxed_type_hash (), (gpointer)type, class);
	g_hash_table_insert (get_boxed_class_hash (), class, (gpointer)type);
}

void
nautilus_mono_marshal_register_enum_type (GType type,
					  MonoAssembly *assembly,
					  const char *namespace,
					  const char *class_name)
{
	MonoClass *class;
	
	g_return_if_fail (G_TYPE_IS_ENUM (type));

	class = mono_class_from_name (assembly->image,
				      namespace,
				      class_name);
	if (!class) {
		g_warning ("couldn't register type for %s.%s", 
			   namespace, class_name);
		return;
	}

	mono_class_init (class);

	g_hash_table_insert (get_enum_type_hash (), (gpointer)type, class);
	g_hash_table_insert (get_enum_class_hash (), class, (gpointer)type);
}

static gboolean
wrap_arg (GValue *value, gpointer *ret)
{
	*ret = 0;
	if (G_VALUE_HOLDS_OBJECT (value)) {
		NautilusMonoHandle *obj;
		obj = wrap_object (g_value_get_object (value));
		*ret = obj->object;
		nautilus_mono_handle_free (obj);
		return TRUE;
	} else if (G_VALUE_HOLDS_BOXED (value)) {
		NautilusMonoHandle *obj;
		obj = wrap_boxed (g_value_get_boxed (value),
				  value->g_type);
		*ret = obj->object;
		nautilus_mono_handle_free (obj);
		return TRUE;
	} else if (G_VALUE_HOLDS_ENUM (value)) {
		*ret = &value->data[0].v_long;
		return TRUE;
	} else {
		switch (value->g_type) {
		case G_TYPE_INT :
			*ret = &value->data[0].v_int;
			return TRUE;
		case G_TYPE_UINT :
			*ret = &value->data[0].v_uint;
			return TRUE;
		case G_TYPE_POINTER :
			*ret = &value->data[0].v_pointer;
			return TRUE;
		default :
			g_warning ("trying to pass unimplemented type to mono\n");
			return FALSE;
		}
	}
	return FALSE;
}
	
static gpointer *
wrap_args (GList *args)
{
	GList *l;
	void **ret;
	int length;
	int i;
	
	if (!args) {
		return NULL;
	}

	length = g_list_length (args);
	ret = g_new0 (gpointer, length);

	for (l = args, i = 0; l != NULL; l = l->next, i++) {
		wrap_arg (l->data, &ret[i]);
	}

	return ret;
}

static GList *
collect_args (GType first_type, va_list var_args)
{
	GList *ret;
	GType type;
	
	ret = NULL;
	type = first_type;
	while (type != 0) {
		GValue *value;
		char *error = NULL;
		
		value = g_new0 (GValue, 1);
		g_value_init (value, type);
		G_VALUE_COLLECT (value, var_args, 0, &error);
		if (!error) {
			ret = g_list_append (ret, value);
		} else {
			g_warning ("%s: %s", G_STRLOC, error);
			g_free (value);
			g_free (error);
		}
		
		type = va_arg (var_args, GType);
	}

	return ret;
}

static void
free_values (GList *values)
{
	GList *l;
	
	for (l = values; l != NULL; l = l->next) {
		g_value_unset (l->data);
		g_free (l->data);
	}
	
	g_list_free (values);
	
}

NautilusMonoHandle *
nautilus_mono_invoke_with_values (NautilusMonoHandle *handle,
				  const char *method_desc,
				  gboolean use_namespace,
				  GList *values)
{
	MonoMethod *method;

	method = lookup_method_desc (handle->object->vtable->klass, 
				     method_desc, TRUE);
	
	if (method) {		
		NautilusMonoHandle *ret;
		gpointer *mono_args;
		int num_args;

		mono_args = wrap_args (values);
		
		ret = invoke_print_exceptions (handle->object, 
					       method,
					       mono_args);
		
		num_args = g_list_length (values);
		memset (mono_args, 0, num_args * sizeof (gpointer));
		g_free (mono_args);

		return ret;
	} else {
		g_warning ("couldn't invoke %s\n", method_desc);
		return NULL;
	}
}

NautilusMonoHandle *
nautilus_mono_invoke (NautilusMonoHandle *handle,
		      const char *method_desc,
		      gboolean use_namespace,
		      GType first_type,
		      ...)
{
	NautilusMonoHandle *ret;
	GList *values;
	va_list args;	

	va_start (args, first_type);

	values = collect_args (first_type, args);
	
	ret = nautilus_mono_invoke_with_values (handle, 
						method_desc, 
						use_namespace,
						values);
	free_values (values);

	va_end (args);

	return ret;
}

		      
