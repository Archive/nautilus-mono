/*
 *  nautilus-mono.c - Nautilus Mono extension
 * 
 *  Copyright (C) 2003, 2004 Novell, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Calvin Gaisford <cgaisford@novell.com>
 *          Dave Camp <dave@ximian.com>
 * 
 */

#include <config.h>
#include "nautilus-mono.h"
#include "nautilus-mono-object.h"
#include "nautilus-mono-helpers.h"

#include <gtk/gtkwindow.h>
#include <gtk/gtkwindow.h>

#include <libnautilus-extension/nautilus-extension-types.h>
#include <libnautilus-extension/nautilus-file-info.h>
                                                                            
#include <mono/jit/jit.h>
#include <mono/metadata/debug-helpers.h>
#include <mono/metadata/threads.h>
#include <mono/metadata/assembly.h>

/* FIXME */
#if 0
#include <mono/metadata/mono-config.h>
#endif
void mono_config_parse (const char *filename);

static GArray *all_types = NULL;



MonoDomain *
nautilus_mono_get_domain (void)
{
	static MonoDomain *nautilus_domain = NULL;
	
	if (!nautilus_domain) {
		nautilus_domain = mono_jit_init("nautilus-mono");
	}

	return nautilus_domain;
}

MonoAssembly *
nautilus_mono_get_nautilus_assembly (void)
{
	static MonoAssembly *nautilus_assembly = NULL;
	
	/* Must hold a reference to nautilus objects on the heap or
	 * the GC gets confused */
	if (!nautilus_assembly) {
		nautilus_assembly = mono_domain_assembly_open (nautilus_mono_get_domain(),
							       LIBDIR "/nautilus-sharp.dll");

	}

	return nautilus_assembly;
}

MonoAssembly *
nautilus_mono_get_glib_assembly (void)
{
	static MonoAssembly *glib_assembly = NULL;
	
	if (!glib_assembly) {
		/* FIXME: check return value */
		glib_assembly = mono_assembly_load_with_partial_name ("glib-sharp", NULL);		
		g_assert (glib_assembly);
	}

	return glib_assembly;
}

MonoAssembly *
nautilus_mono_get_gtk_assembly (void)
{
	static MonoAssembly *gtk_assembly = NULL;
	
	if (!gtk_assembly) {
		/* FIXME: check status */
		gtk_assembly = mono_assembly_load_with_partial_name ("gtk-sharp", NULL);		
	}

	return gtk_assembly;
}

static void
register_types (GTypeModule *module, 
		MonoArray *types)
{
	int i;
	
	for (i = 0; i < mono_array_length (types); i++) {
		GType type;
		MonoReflectionType *reflection_type;

		/* Don't have to heap allocate the reflection type 
		 * pointer, as the MonoArray* is already holding a ref
		 * on the heap */
		reflection_type = mono_array_get (types,
						  MonoReflectionType*, 
						  i);
		
		type = nautilus_mono_object_get_type 
			(module, reflection_type->type);

		g_array_append_val (all_types, type);
		
		g_print ("registered a type\n");		
	}
}

static void
load_assembly (GTypeModule *module, const char *filename)
{
	MonoAssembly *assembly;
	g_print ("loading %s\n", filename);

	assembly = mono_domain_assembly_open (nautilus_mono_get_domain(),
					      filename);

	if (assembly) {
		MonoMethodDesc *desc;

		desc = mono_method_desc_new("Nautilus.Extension:GetTypes()",
					    TRUE);
		
		if (desc) {
			MonoMethod *method;
			
			method = mono_method_desc_search_in_image 
				(desc, assembly->image);
			if (method) {
				MonoObject *ex;
				MonoObject *types;
				
				types = mono_runtime_invoke(method, NULL, NULL, &ex);
				
				if (!ex) {
					register_types (module, (MonoArray*)types);
					g_print ("loaded %s\n", filename);
				} else {
					mono_print_unhandled_exception (ex);
				}
			} else {
				g_print ("did not have Nautilus.Extension:GetTypes()\n");
			}
		} else {
			g_print ("couldn't create method desc\n");
		}
	} else {
		g_print ("Couldn't load assembly\n");
	}
}

static gpointer 
nautilus_object_list_copy (gpointer boxed)
{
	GList *copy;
	GList *l;
	
	copy = g_list_copy (boxed);
	
	for (l = copy; l != NULL; l = l->next) {
		g_object_ref (l->data);
	}
	
	return copy;
}

static void
nautilus_object_list_free (gpointer boxed)
{
	GList *l;
	
	for (l = boxed; l != NULL; l = l->next) {
		g_object_unref (l->data);
	}

	g_list_free (boxed);	
}

GType
nautilus_object_list_get_type (void)
{
	static GType type = 0;
	
	if (type == 0) {
		type = g_boxed_type_register_static 
			("NautilusObjectList",
			 nautilus_object_list_copy,
			 nautilus_object_list_free);
	}
	
	return type;
}

static GList *
load_dir (const char *dirname)
{
	GDir *dir;
	GList *ret;

	ret = NULL;
	
	dir = g_dir_open (dirname, 0, NULL);
	
	if (dir) {
		const char *name;
		
		while ((name = g_dir_read_name (dir))) {
			if (g_str_has_suffix (name, ".dll")) {
				char *filename;
				
				filename = g_build_filename (dirname, name, NULL);
				ret = g_list_prepend (ret, filename);
			}
		}
		g_dir_close (dir);
	}

	ret = g_list_reverse (ret);

	return ret;
}

static gboolean initialized_mono = FALSE;

void
nautilus_module_initialize (GTypeModule  *module)
{
	GList *filenames;
	GList *l;
	
	g_print ("Initializing nautilus-mono extension\n");

	all_types = g_array_new (FALSE, FALSE, sizeof (GType));

	filenames = load_dir (LIBDIR "/nautilus/extensions-1.0/mono");
	if (!filenames) {
		g_print ("no mono extensions\n");
		
		return;
	}

	g_print ("  Loading up the mono jit...\n");


	initialized_mono = TRUE;
	
	mono_thread_attach (nautilus_mono_get_domain ());

	/* FIXME - is this right? */
	mono_config_parse (NULL);

 	nautilus_mono_marshal_register_boxed_type (NAUTILUS_TYPE_OBJECT_LIST,
						   nautilus_mono_get_glib_assembly (),
						   "GLib",
						   "List");

 	nautilus_mono_marshal_register_enum_type (NAUTILUS_TYPE_OPERATION_RESULT,
						  nautilus_mono_get_nautilus_assembly (),
						  "Nautilus",
						  "OperationResult");


	if (nautilus_mono_get_nautilus_assembly ()) {
		for (l = filenames; l != NULL; l = l->next) {
			load_assembly (module, l->data);
			g_free (l->data);
		}
		g_list_free (filenames);
	} else {
		g_warning ("Couldn't load nautilus.dll");
	}

	g_print ("Done initializing\n");
}

/* Perform module-specific shutdown. */
void
nautilus_module_shutdown   (void)
{
	if (initialized_mono) {
		mono_jit_cleanup(nautilus_mono_get_domain ());
	}

	g_print ("Shutting down nautilus-mono extension\n");
}

/* List all the extension types.  */
void 
nautilus_module_list_types (const GType **types,
			    int          *num_types)
{
	*types = (GType*)all_types->data;
	*num_types = all_types->len;
}

