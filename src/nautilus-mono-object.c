/*
 *  nautilus-mono-object.h - Generation of wrapper objects for nautilus 
 *                           extension objects in mono.
 * 
 *  Copyright (C) 2004 Novell, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dave Camp <dave@ximian.com>
 * 
 */

#include <config.h>
#include <nautilus-mono-object.h>

/* Nautilus extension headers */
#include <libnautilus-extension/nautilus-extension-types.h>
#include <libnautilus-extension/nautilus-file-info.h>
#include <libnautilus-extension/nautilus-info-provider.h>
#include <libnautilus-extension/nautilus-column-provider.h>
#include <libnautilus-extension/nautilus-menu-provider.h>
#include <libnautilus-extension/nautilus-property-page-provider.h>

#include <mono/jit/jit.h>
#include <mono/metadata/debug-helpers.h>
#include <mono/metadata/threads.h>
#include <mono/metadata/tabledefs.h>

#include <gtk/gtklabel.h>

#include <string.h>

#include "nautilus-mono.h"
#include "nautilus-mono-helpers.h"

static GObjectClass *parent_class;

#define OBJECT_CLASS(o)((NautilusMonoObjectClass*)(((GTypeInstance*)(o))->g_class))
#define OBJECT_MONO_CLASS(o)(((NautilusMonoObjectClass*)(((GTypeInstance*)(o))->g_class))->class)

static GList *
nautilus_mono_object_get_property_pages (NautilusPropertyPageProvider *provider,
					 GList *files)
{
	NautilusMonoObject *object;
	NautilusMonoHandle *ret;
	GValue ret_value = {0, };

	object = (NautilusMonoObject*)provider;
	
	ret = nautilus_mono_invoke (object->handle,
				    "Nautilus.PropertyPageProvider:GetPages(GLib.List)",
				    TRUE,
				    NAUTILUS_TYPE_OBJECT_LIST,
				    files,
				    0);
	if (ret && ret->object) {
		nautilus_mono_handle_get_value (ret, &ret_value);
		nautilus_mono_handle_free (ret);
		return g_value_get_boxed (&ret_value);
	} else {
		nautilus_mono_handle_free (ret);
		return NULL;
	}
}

static void
nautilus_mono_object_property_page_provider_iface_init (NautilusPropertyPageProviderIface *iface)
{
	iface->get_pages = nautilus_mono_object_get_property_pages;
}

static GList *
nautilus_mono_object_get_file_items (NautilusMenuProvider *provider,
				     GtkWidget *window,
				     GList *files)
{
	NautilusMonoObject *object;
	NautilusMonoHandle *ret;
	GValue ret_value = { 0, };

	object = (NautilusMonoObject*)provider;
	
	ret = nautilus_mono_invoke (object->handle,
				    "Nautilus.MenuProvider:GetFileItems(Gtk.Widget,GLib.List)",
				    TRUE,
				    GTK_TYPE_WINDOW,
				    window,
				    NAUTILUS_TYPE_OBJECT_LIST,
				    files,
				    0);
	if (ret && ret->object) {
		nautilus_mono_handle_get_value (ret, &ret_value);
		nautilus_mono_handle_free (ret);
		return g_value_get_boxed (&ret_value);
	} else {
		nautilus_mono_handle_free (ret);
		return NULL;
	}
}

static GList *
nautilus_mono_object_get_background_items (NautilusMenuProvider *provider,
					   GtkWidget *window,
					   NautilusFileInfo *file)
{
	NautilusMonoObject *object;
	NautilusMonoHandle *ret;
	GValue ret_value = { 0, };

	object = (NautilusMonoObject*)provider;
	
	ret = nautilus_mono_invoke (object->handle,
				    "Nautilus.MenuProvider:GetBackgroundItems(Gtk.Widget,Nautilus.FileInfo)",
				    TRUE,
				    GTK_TYPE_WINDOW,
				    window,
				    NAUTILUS_TYPE_FILE_INFO,
				    file,
				    0);
	if (ret && ret->object) {
		nautilus_mono_handle_get_value (ret, &ret_value);
		nautilus_mono_handle_free (ret);
		return g_value_get_boxed (&ret_value);
	} else {
		nautilus_mono_handle_free (ret);
		return NULL;
	}
}


static GList *
nautilus_mono_object_get_toolbar_items (NautilusMenuProvider *provider,
					GtkWidget *window,
					NautilusFileInfo *file)
{
	NautilusMonoObject *object;
	NautilusMonoHandle *ret;
	GValue ret_value = { 0, };

	object = (NautilusMonoObject*)provider;
	
	ret = nautilus_mono_invoke (object->handle,
				    "Nautilus.MenuProvider:GetToolbarItems(Gtk.Widget,Nautilus.FileInfo)",
				    TRUE,
				    GTK_TYPE_WINDOW,
				    window,
				    NAUTILUS_TYPE_FILE_INFO,
				    file,
				    0);
	if (ret && ret->object) {
		nautilus_mono_handle_get_value (ret, &ret_value);
		nautilus_mono_handle_free (ret);
		return g_value_get_boxed (&ret_value);
	} else {
		nautilus_mono_handle_free (ret);
		return NULL;
	}
}

static void
nautilus_mono_object_menu_provider_iface_init (NautilusMenuProviderIface *iface)
{
	iface->get_background_items = nautilus_mono_object_get_background_items;
	iface->get_toolbar_items = nautilus_mono_object_get_toolbar_items;
	iface->get_file_items = nautilus_mono_object_get_file_items;
}


static GList *
nautilus_mono_object_get_columns (NautilusColumnProvider *provider)
{
	NautilusMonoObject *object;
	NautilusMonoHandle *ret;
	GValue ret_value = { 0, };

	object = (NautilusMonoObject*)provider;
	
	ret = nautilus_mono_invoke (object->handle,
				    "Nautilus.ColumnProvider:GetColumns()",
				    TRUE,
				    0);
	if (ret && ret->object) {
		nautilus_mono_handle_get_value (ret, &ret_value);
		nautilus_mono_handle_free (ret);
		return g_value_get_boxed (&ret_value);
	} else {
		nautilus_mono_handle_free (ret);
		return NULL;
	}
}

static void
nautilus_mono_object_column_provider_iface_init (NautilusColumnProviderIface *iface)
{
	iface->get_columns = nautilus_mono_object_get_columns;
}

static void
nautilus_mono_object_cancel_update (NautilusInfoProvider *provider,
				    NautilusOperationHandle *handle)
{
	NautilusMonoObject *object;
	NautilusMonoHandle *ret;

	object = (NautilusMonoObject*)provider;
	
	ret = nautilus_mono_invoke (object->handle,
				    "Nautilus.InfoProvider:CancelUpdate(Nautilus.OperationHandle)",
				    TRUE,
				    G_TYPE_POINTER,
				    handle,
				    0);
	nautilus_mono_handle_free (ret);
}

static NautilusOperationResult
nautilus_mono_object_update_file_info (NautilusInfoProvider *provider,
				       NautilusFile *file,
				       GClosure *update_complete,
				       NautilusOperationHandle **handle)
{
	NautilusMonoObject *object;
	NautilusMonoHandle *ret;
	GValue ret_value = { 0, };

	object = (NautilusMonoObject*)provider;
	
	ret = nautilus_mono_invoke (object->handle,
				    "Nautilus.InfoProvider:UpdateFileInfo(Nautilus.FileInfo,intptr,Nautilus.OperationHandle)",
				    TRUE,
				    NAUTILUS_TYPE_FILE_INFO,
				    file,
				    G_TYPE_POINTER,
				    update_complete,
				    G_TYPE_POINTER,
				    NULL,
				    0);

	if (ret && ret->object) {
		nautilus_mono_handle_get_value (ret, &ret_value);
		nautilus_mono_handle_free (ret);
		return g_value_get_enum (&ret_value);
	} else {
		nautilus_mono_handle_free (ret);
		return NAUTILUS_OPERATION_COMPLETE;
	}
}

static void
nautilus_mono_object_info_provider_iface_init (NautilusInfoProviderIface *iface)
{
	iface->cancel_update = nautilus_mono_object_cancel_update;
	iface->update_file_info = nautilus_mono_object_update_file_info;
}

static void 
nautilus_mono_object_instance_init (NautilusMonoObject *object)
{
	NautilusMonoObjectClass *class;
	char *constructor;
	NautilusMonoHandle *constructor_ret;
	
	class = (NautilusMonoObjectClass*)(((GTypeInstance*)object)->g_class);

	object->handle = nautilus_mono_handle_new ();
	object->handle->object = mono_object_new (nautilus_mono_get_domain (),
						  class->class);

	constructor = g_strdup_printf ("%s::.ctor()", class->class->name);
	constructor_ret = nautilus_mono_invoke (object->handle, 
						constructor, 
						FALSE,
						0);
	nautilus_mono_handle_free (constructor_ret);
	g_free (constructor);
}

static void
nautilus_mono_object_finalize (GObject *obj)
{
	NautilusMonoObject *object;
	
	object = (NautilusMonoObject*)obj;
	
	nautilus_mono_handle_free (object->handle);
}

static void
nautilus_mono_object_class_init (NautilusMonoObjectClass *class,
				 gpointer class_data)
{
	parent_class = g_type_class_peek_parent (class);
	
	class->class = (MonoClass*)class_data;

	G_OBJECT_CLASS (class)->finalize = nautilus_mono_object_finalize;
}

static void
canonicalize_name (char *key)
{
	char *p;
   
	for (p = key; *p != 0; p++) {
		char c = *p;
		
		if (c != '-' &&
		    (c < '0' || c > '9') &&
		    (c < 'A' || c > 'Z') &&
		    (c < 'a' || c > 'z'))
			*p = '-';
	}
}

GType 
nautilus_mono_object_get_type (GTypeModule *module, 
			       MonoType *type)
{
	GTypeInfo *info;
	char *type_name;
	GType gtype;

	static const GInterfaceInfo property_page_provider_iface_info = {
		(GInterfaceInitFunc) nautilus_mono_object_property_page_provider_iface_init,
		NULL,
		NULL
	};

	static const GInterfaceInfo menu_provider_iface_info = {
		(GInterfaceInitFunc) nautilus_mono_object_menu_provider_iface_init,
		NULL,
		NULL
	};

	static const GInterfaceInfo column_provider_iface_info = {
		(GInterfaceInitFunc) nautilus_mono_object_column_provider_iface_init,
		NULL,
		NULL
	};

	static const GInterfaceInfo info_provider_iface_info = {
		(GInterfaceInitFunc) nautilus_mono_object_info_provider_iface_init,
		NULL,
		NULL
	};


	info = g_new0 (GTypeInfo, 1);
	
	info->class_size = sizeof (NautilusMonoObjectClass);
	info->class_init = (GClassInitFunc)nautilus_mono_object_class_init;
	info->instance_size = sizeof (NautilusMonoObject);
	info->instance_init = (GInstanceInitFunc)nautilus_mono_object_instance_init;
	
	info->class_data = mono_class_from_mono_type (type);
	mono_class_init ((MonoClass*)info->class_data);

	type_name = mono_type_get_name (type);
	canonicalize_name (type_name);
	gtype = g_type_module_register_type (module, 
					     G_TYPE_OBJECT,
					     type_name,
					     info, 0);

	g_type_module_add_interface (module, gtype, 
				     NAUTILUS_TYPE_PROPERTY_PAGE_PROVIDER,
				     &property_page_provider_iface_info);

	g_type_module_add_interface (module, gtype, 
				     NAUTILUS_TYPE_MENU_PROVIDER,
				     &menu_provider_iface_info);

	g_type_module_add_interface (module, gtype, 
				     NAUTILUS_TYPE_COLUMN_PROVIDER,
				     &column_provider_iface_info);

	g_type_module_add_interface (module, gtype, 
				     NAUTILUS_TYPE_INFO_PROVIDER,
				     &info_provider_iface_info);

	return gtype;
}
