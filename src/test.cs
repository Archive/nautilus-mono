using System;
using Nautilus;
using Gtk;
using System.Collections;

namespace Test {

	public class TestPropertyPage : Gtk.VBox {
		public TestPropertyPage (Nautilus.FileInfo file) : base (false, 0) {
			Console.WriteLine ("adding property page for " + file.Name);
			Label label = new Label ("File: " + file.Name);
			label.Show ();
			PackStart (label, false, false, 0);
		}
	}

	public class Test : GLib.Object, 
		            Nautilus.PropertyPageProvider, 
		            Nautilus.MenuProvider,
	                    Nautilus.ColumnProvider,
	                    Nautilus.InfoProvider {
		public Test () {
			System.Console.WriteLine ("Instantiated Test.Test");
		}
		
		// Nautilus.PropertyPageProvider interface
		public GLib.List GetPages (GLib.List files) 
		{
			// Only try to support single-file property pages
			if (files.Count > 1) {
				return null;
			}
			
			GLib.List ret = new GLib.List 
				((IntPtr) 0, typeof (Nautilus.PropertyPage));

			Widget widget = new TestPropertyPage
				((Nautilus.FileInfo)files[0]);
			widget.Show ();
				
			Nautilus.PropertyPage page = new Nautilus.PropertyPage
				("NautilusMonoTest::property_page",
				 new Label ("Mono Test"),
				 widget);
			
			ret.Append (page.Handle);

			return ret;
		}

		// Nautilus.MenuProvider interface
		public GLib.List GetToolbarItems (Widget window,
						  FileInfo current_folder)
		{
			System.Console.WriteLine ("get toolbar items for " + current_folder.Name);

			return null;
		}

		private void HandleFileActivated (object sender, EventArgs args)
		{
			Nautilus.MenuItem item = (Nautilus.MenuItem)sender;
			System.Console.WriteLine ("Menuitem Activated");
			ArrayList files = (ArrayList)item.Data["files"];
			foreach (Nautilus.FileInfo file in files) {
				System.Console.WriteLine (file.Name);
			}
		}

		public GLib.List GetFileItems (Gtk.Widget window,
					       GLib.List files)
		{
			GLib.List ret = new GLib.List 
				((IntPtr) 0, typeof (Nautilus.MenuItem));
			Nautilus.MenuItem item = new Nautilus.MenuItem
				("NautilusMonoTest::print_files_item",
				 "Test Mono Extensions",
				 "Print all filenames to the console",
				 "gnome-fs-executable");

			ArrayList data = new ArrayList ();
			foreach (Nautilus.FileInfo file in files) {
				data.Add (file);
			}
			item.Data["files"] = data;
				
			item.Activated += new EventHandler (HandleFileActivated);
			ret.Append (item.Handle);

			System.Console.WriteLine ("get file items ");
			return ret;
		}

		public GLib.List GetBackgroundItems (Widget window,
						     FileInfo current_folder)
		{
			System.Console.WriteLine ("get background items current folder: " + current_folder.Name);
			return null;
		}

		// ColumnProvider interface
		public GLib.List GetColumns () {
			GLib.List ret = new GLib.List 
				((IntPtr) 0, typeof (Nautilus.Column));
			
			Nautilus.Column column = new Nautilus.Column
				("NautilusMonoTest::mono_column",
				 "mono_data",
				 "Mono Data",
				 "Information from the Mono test component");
			
			ret.Append (column.Handle);
			
			return ret;
		}

		// InfoProvider interface
		public void CancelUpdate (Nautilus.OperationHandle handle) {
		}

		public Nautilus.OperationResult UpdateFileInfo (Nautilus.FileInfo file,
								IntPtr update_complete,
								Nautilus.OperationHandle handle) {
			file.AddStringAttribute ("mono_data",
						 "testing " + file.Name);
			return Nautilus.OperationResult.Complete;
		}
	}
}

namespace Nautilus {
	class Extension {
		public static System.Type[] GetTypes () 
		{
			System.Console.WriteLine ("Getting classes");
			System.Type[] ret = new System.Type[1];	
			ret[0] = typeof (Test.Test);
			return ret;
		}
	}
}
