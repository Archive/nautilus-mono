/*
 *  nautilus-mono-object.h - Generation of wrapper objects for nautilus 
 *                           extension objects in mono.
 * 
 *  Copyright (C) 2003 Novell, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dave Camp <dave@ximian.com>
 * 
 */

#ifndef NAUTILUS_MONO_OBJECT_H
#define NAUTILUS_MONO_OBJECT_H

#include <glib-object.h>
#include <mono/jit/jit.h>
#include "nautilus-mono-helpers.h"

G_BEGIN_DECLS

typedef struct _NautilusMonoObject       NautilusMonoObject;
typedef struct _NautilusMonoObjectClass  NautilusMonoObjectClass;

struct _NautilusMonoObject {
	GObject parent_slot;
	
	NautilusMonoHandle *handle;
};

struct _NautilusMonoObjectClass {
	GObjectClass parent_slot;

	MonoClass *class;
};

GType nautilus_mono_object_get_type (GTypeModule *module, 
				     MonoType *class);

G_END_DECLS

#endif
